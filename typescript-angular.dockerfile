FROM alpine:3.18.3

# openapi
RUN apk add --no-cache \
        npm=9.6.6-r0 \
        openjdk17-jre-headless=17.0.8_p7-r0 && \
    npm install @openapitools/openapi-generator-cli@2.7.0 -g

# typescript-angular
RUN apk add --no-cache \
        npm=9.6.6-r0
